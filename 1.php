 <?php

trait message1 {

  public function msg1() {
    $Date1= new DateTime("1981-11-03");
    $Date2 = new DateTime("2013-09-04");
    $difference = $Date1->diff($Date2);
    echo "<center>";
    echo "<br>";
    echo "First Date: 1981-11-03";
    echo "<br>";
    echo "Second Date: 2013-09-04";
    echo "<br>";
    echo "Difference : " . $difference->y . " years, " . $difference->m." months, ".$difference->d." days ";
    echo "</center>";
  }

}

class Welcome {
  use message1;
}

$obj2 = new Welcome();
$obj2->msg1();


 
?> 

